
doc:
	R -s -e "pkgload::load_all();roxygen2::roxygenize()"

pkg: doc
	R CMD build ./

check: pkg
	R CMD check uwe*.tar.gz

cran:  pkg
	R CMD check --as-cran uwe*tar.gz

test: doc
	R -s -e "tinytest::build_install_test()"

manual: doc
	R CMD Rd2pdf --force -o manual.pdf ./

clean:
	rm *.tar.gz
	rm -rf *.Rcheck
	rm -rf manual.pdf

